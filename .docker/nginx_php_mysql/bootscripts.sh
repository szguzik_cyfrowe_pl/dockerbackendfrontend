#!/bin/sh

echo "Starting php"
service php7.4-fpm start

echo "Starting mysql"
usermod -d /var/lib/mysql/ mysql
service mysql start
mysql -uroot -e "CREATE USER 'user'@'%' IDENTIFIED BY 'apps123'"
mysql -uroot -e "GRANT ALL PRIVILEGES ON *.* TO 'user'@'%' WITH GRANT OPTION"
mysql -uroot -e "FLUSH PRIVILEGES"
mysql -uroot -e "FLUSH PRIVILEGES"
mysql -uroot -e "CREATE DATABASE project_db"

echo "Starting nginx"
nginx -g "daemon off;"

